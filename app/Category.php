<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Category;
use App\SubCategory;
use App\Product;

class Category extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'url_name', 'icon', 'status', 'sl_date', 'content'];

    public function getRouteKeyName()
    {
    	return 'url_name';
    }

    public function subCategory()
    {
    	return $this->hasMany(SubCategory::class);
    }

    public function product()
    {
    	return $this->hasMany(Product::class);
    }

}
