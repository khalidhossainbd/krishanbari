<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Special extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'specials';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'url_name', 'images', 'unites', 'price', 'productId', 'status', 'sl_date', 'content'];

    
}
