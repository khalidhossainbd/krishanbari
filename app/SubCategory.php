<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sub_categories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['title', 'url_name', 'status', 'sl_date', 'category_id', 'icon', 'content'];

    public function getRouteKeyName()
    {
    	return 'url_name';
    }

    public function category()
    {
    	return $this->belongsTo(Category::class);
    }

    public function product()
    {
    	return $this->hasMany(Product::class, 'subcategory_id', 'id');
    }
}
