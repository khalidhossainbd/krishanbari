<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Special;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Redirect,Response;
use Auth;

class SpecialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        $myproducts = Special::all();
        return view('admin.pages.special.index', compact('myproducts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.special.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
                'title' => ['required', 'string', 'max:255'],
                'url_name' =>['required', 'string', 'regex:/^([-a-z0-9_ ])+$/i', 'unique:specials', 'alpha_dash', 'max:255'],
                'status'=>['required','string'],
                'unites'=>['required','string'],
                'productId'=>['required'],
                'sl_date'=>['required'],
                'price'=>['required'],
                'content'=>['string'],
                'images'=>['required'],
                ]);

        $file = $request->file('images');

        // dd($file);

        if (!empty($file)){

            $image = Image::make($file);

            $path = 'uploads/products/';

            $fileName = $request->file('images')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).time().".".$filetype;
          
            $image->save($path.$filenewname);
            
            $image->resize(400, 400);
            $image->save($path.$filenewname);
               
            $myproduct = new Special();

            $myproduct->title = $request->title;
            $myproduct->url_name = $request->url_name;
            $myproduct->sl_date = $request->sl_date;
            $myproduct->unites = $request->unites;
            $myproduct->productId = $request->productId;
            $myproduct->price = $request->price;
            $myproduct->status = $request->status;
            $myproduct->content = $request->content;
            $myproduct->images = $filenewname;

            $myproduct->save();

        }else{
            $requestData = $request->all();
            Special::create($requestData);
        }

        return Redirect::to("/admin/specialproduct")->withSuccess('Great! Product Add successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Special  $special
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $myproduct=Special::findOrFail($id);
        return view('admin.pages.special.show', compact('myproduct'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Special  $special
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $myproduct=Special::findOrFail($id);
        return view('admin.pages.special.edit', compact('myproduct'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Special  $special
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $request->validate([
                'title' => ['required', 'string', 'max:255'],
                'url_name' =>['required', 'string', 'regex:/^([-a-z0-9_ ])+$/i', 'alpha_dash', 'max:255'],
                'status'=>['required','string'],
                'unites'=>['required','string'],
                'productId'=>['required'],
                'price'=>['required'],
                'content'=>['string'],
                ]);

            $file = $request->file('images');

          

            if (!empty($file)){
            
            $sproduct = Special::find($id);
           
            $pathToImage = 'uploads/products/'.$sproduct['images'];

            $image = Image::make($file);

            $path = 'uploads/products/';

            $fileName = $request->file('images')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).time().".".$filetype;
            
            //$image->save($path.$filenewname);
            
            $image->resize(400, 400);
            $image->save($path.$filenewname);

            if($image->save()==true){
                File::delete($pathToImage);
            }

            $sproduct->title = $request->title;
            $sproduct->url_name = $request->url_name;
            $sproduct->sl_date = $request->sl_date;
            $sproduct->unites = $request->unites;
            $sproduct->productId = $request->productId;
            $sproduct->price = $request->price;
            $sproduct->status = $request->status;
            $sproduct->content = $request->content;
            $sproduct->images = $filenewname;
            $sproduct->update();

        }else{
            $mycategory = Special::findOrFail($id);
            $mycategory->update($request->all());
        }

        return Redirect::to("/admin/specialproduct")->withSuccess('Great! Product info updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Special  $special
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mypoem = Special::find($id);

        if($mypoem['images']){
            $pathToImage = 'uploads/products/'.$mypoem['images'];

            File::delete($pathToImage);
        }
        $mypoem->delete();
        
        return redirect('/admin/specialproduct')->with('success', 'Product deleted!');
    }
}
