<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use App\Http\Controllers\Controller;

use Redirect,Response;
use App\Category;
use Auth;


class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    public function index()
    {
        $category = Category::all();
        return view('admin.pages.category.index', compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
                'title' => ['required', 'string', 'max:255'],
                'url_name' =>['required', 'string', 'regex:/^([-a-z0-9_ ])+$/i', 'unique:categories', 'alpha_dash', 'max:255'],
                'sl_date'=>['required'],
                'status'=>['required','string'],
                'content'=>['string'],
                'icon'=>['required'],
                ]);

        $file = $request->file('icon');

        if (!empty($file)){

            $image = Image::make($file);

            $path = 'uploads/icons/';

            $fileName = $request->file('icon')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).time().".".$filetype;
          
            $image->save($path.$filenewname);
            
            $image->resize(150, 150);
            $image->save($path.$filenewname);

            $category = new Category();
            $category->title = $request->title;
            $category->url_name = $request->url_name;
            $category->sl_date = $request->sl_date;
            $category->status = $request->status;
            $category->content = $request->content;
            $category->icon = $filenewname;
            $category->save();

        }else{
            $requestData = $request->all();
            Category::create($requestData);
        }

        return Redirect::to("/admin/category")->withSuccess('Great! Category Add successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Redirect::to("/admin/category");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        return view('admin.pages.category.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);

        $request->validate([
                'title' => ['required', 'string', 'max:255'],
                'url_name' =>['required', 'string', 'regex:/^[\p{L}\s-]+$/u', 'alpha_dash', 'max:255'],
                'sl_date'=>['required'],
                'status'=>['required','string'],
                'content'=>['string'],
                ]);

            $file = $request->file('icon');

          

            if (!empty($file)){
            
            $mycategory = Category::find($id);
           
            $pathToImage = 'uploads/icons/'.$mycategory['icon'];

            $image = Image::make($file);

            $path = 'uploads/icons/';

            $fileName = $request->file('icon')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
            
            //$image->save($path.$filenewname);
            
            $image->resize(150, 150);
            $image->save($path.$filenewname);

            if($image->save()==true){
                File::delete($pathToImage);
            }

            $mycategory->title = $request->title;
            $mycategory->url_name = $request->url_name;
            $mycategory->sl_date = $request->sl_date;
            $mycategory->status = $request->status;
            $mycategory->content = $request->content;
            $mycategory->icon = $filenewname;
            $mycategory->update();

        }else{
            $mycategory = Category::findOrFail($id);
            $mycategory->update($request->all());
        }

        return Redirect::to("/admin/category")->withSuccess('Great! Category info updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
