<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use Redirect,Response;
use Auth;

use App\Category;
use App\SubCategory;
use App\Product;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index()
    {
        $products = Product::all();
        return view('admin.pages.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::all(); 
        return view('admin.pages.product.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
                'title' => ['required', 'string', 'max:255'],
                'url_name' =>['required', 'string', 'regex:/^([-a-z0-9_ ])+$/i', 'unique:products', 'alpha_dash', 'max:255'],
                'category_id' =>['required'],
                'subcategory_id' =>['required'],
                'status'=>['required','string'],
                'unites'=>['required','string'],
                'productId'=>['required'],
                'price'=>['required'],
                'content'=>['string'],
                'images'=>['required'],
                ]);

        $file = $request->file('images');

        // dd($file);

        if (!empty($file)){

            $image = Image::make($file);

            $path = 'uploads/products/';

            $fileName = $request->file('images')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).time().".".$filetype;
          
            $image->save($path.$filenewname);
            
            $image->resize(400, 400);
            $image->save($path.$filenewname);
               
            $myproduct = new Product();

            $myproduct->title = $request->title;
            $myproduct->url_name = $request->url_name;
            $myproduct->category_id = $request->category_id;
            $myproduct->subcategory_id = $request->subcategory_id;
            $myproduct->unites = $request->unites;
            $myproduct->productId = $request->productId;
            $myproduct->price = $request->price;
            $myproduct->last_price = $request->last_price;
            $myproduct->status = $request->status;
            $myproduct->content = $request->content;
            $myproduct->images = $filenewname;

            $myproduct->save();

        }else{
            $requestData = $request->all();
            Product::create($requestData);
        }

        return Redirect::to("/admin/product")->withSuccess('Great! Product Add successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $myproduct=Product::findOrFail($id);
        return view('admin.pages.product.show', compact('myproduct'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $myproduct=Product::findOrFail($id);
        $category = Category::all(); 
        return view('admin.pages.product.edit', compact('myproduct', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $request->validate([
                'title' => ['required', 'string', 'max:255'],
                'url_name' =>['required', 'string', 'regex:/^([-a-z0-9_ ])+$/i', 'alpha_dash', 'max:255'],
                'category_id' =>['required'],
                'subcategory_id' =>['required'],
                'status'=>['required','string'],
                'unites'=>['required','string'],
                'productId'=>['required'],
                'price'=>['required'],
                'content'=>['string'],
                ]);

            $file = $request->file('images');

          

            if (!empty($file)){
            
            $sproduct = Product::find($id);
           
            $pathToImage = 'uploads/products/'.$sproduct['images'];

            $image = Image::make($file);

            $path = 'uploads/products/';

            $fileName = $request->file('images')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).time().".".$filetype;
            
            //$image->save($path.$filenewname);
            
            $image->resize(400, 400);
            $image->save($path.$filenewname);

            if($image->save()==true){
                File::delete($pathToImage);
            }

            $sproduct->title = $request->title;
            $sproduct->url_name = $request->url_name;
            $sproduct->category_id = $request->category_id;
            $sproduct->subcategory_id = $request->subcategory_id;
            $sproduct->unites = $request->unites;
            $sproduct->productId = $request->productId;
            $sproduct->price = $request->price;
            $sproduct->last_price = $request->last_price;
            $sproduct->status = $request->status;
            $sproduct->content = $request->content;
            $sproduct->images = $filenewname;
            $sproduct->update();

        }else{
            $mycategory = product::findOrFail($id);
            $mycategory->update($request->all());
        }

        return Redirect::to("/admin/product")->withSuccess('Great! Product info updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mypoem = Product::find($id);

        if($mypoem['images']){
            $pathToImage = 'uploads/products/'.$mypoem['images'];

            File::delete($pathToImage);
        }
        $mypoem->delete();
        
        return redirect('/admin/product')->with('success', 'Product deleted!');
    }

    public function getSubCategory(Request $request)
    {
        $data = SubCategory::select('id', 'title')->where('category_id', $request->id)->get();

        return response()->json($data);
    }
}
