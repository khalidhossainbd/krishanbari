<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use File;
use Image;
use Session;
use App\Http\Controllers\Controller;

use Redirect,Response;
use Auth;

use App\Category;
use App\SubCategory;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin');
    }
    
    public function index()
    {
        $subcategory= SubCategory::all();
        return view('admin.pages.subcategory.index', compact('subcategory'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category= Category::all();
        return view('admin.pages.subcategory.create', compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // dd($request);
        $request->validate([
                'title' => ['required', 'string', 'max:255'],
                'url_name' =>['required', 'string', 'regex:/^[\p{L}\s-]+$/u', 'unique:sub_categories', 'alpha_dash', 'max:255'],
                'sl_date'=>['required'],
                'category_id' =>['required'],
                'status'=>['required','string'],
                'content'=>['string'],
                'icon'=>['required'],
                ]);

        $file = $request->file('icon');

        if (!empty($file)){

            $image = Image::make($file);

            $path = 'uploads/icons/';

            $fileName = $request->file('icon')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).time().".".$filetype;
          
            $image->save($path.$filenewname);
            
            $image->resize(150, 150);
            $image->save($path.$filenewname);

            $subcategory = new SubCategory();
            $subcategory->title = $request->title;
            $subcategory->url_name = $request->url_name;
            $subcategory->category_id = $request->category_id;
            $subcategory->sl_date = $request->sl_date;
            $subcategory->status = $request->status;
            $subcategory->content = $request->content;
            $subcategory->icon = $filenewname;
            $subcategory->save();

        }else{
            $requestData = $request->all();
            SubCategory::create($requestData);
        }

        return Redirect::to("/admin/subcategory")->withSuccess('Great! SubCategory Add successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category= Category::all();
        $subcategory = SubCategory::findOrFail($id);
        return view('admin.pages.subcategory.edit', compact('category', 'subcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request);
        $request->validate([
                'title' => ['required', 'string', 'max:255'],
                'url_name' =>['required', 'string', 'regex:/^[\p{L}\s-]+$/u', 'alpha_dash', 'max:255'],
                'sl_date'=>['required'],
                'category_id' =>['required'],
                'status'=>['required','string'],
                'content'=>['string'],
                ]);

            $file = $request->file('icon');

          

            if (!empty($file)){
            
            $scategory = SubCategory::find($id);
           
            $pathToImage = 'uploads/icons/'.$scategory['icon'];

            $image = Image::make($file);

            $path = 'uploads/icons/';

            $fileName = $request->file('icon')->getClientOriginalName();
            $extension = explode(".", strtolower($fileName));
            $filetype = end($extension);
            $filenewname = rand(1000000, 999999999).".".$filetype;
            
            //$image->save($path.$filenewname);
            
            $image->resize(150, 150);
            $image->save($path.$filenewname);

            if($image->save()==true){
                File::delete($pathToImage);
            }

            $scategory->title = $request->title;
            $scategory->url_name = $request->url_name;
            $scategory->category_id = $request->category_id;
            $scategory->sl_date = $request->sl_date;
            $scategory->status = $request->status;
            $scategory->content = $request->content;
            $scategory->icon = $filenewname;
            $scategory->update();

        }else{
            $mycategory = SubCategory::findOrFail($id);
            $mycategory->update($request->all());
        }

        return Redirect::to("/admin/subcategory")->withSuccess('Great! SubCategory info updated successfully.');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
