<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\SubCategory;
use App\Special;


class MainController extends Controller
{

	public function index()
	{
		$subcategory = SubCategory::where('status', '=', 'Active')->take(7)->get();
		return view('mainpages.home', compact('subcategory'));
	}


    public function products_category($urlname)
    {
    	$category = Category::where('url_name', '=', $urlname)->get();
    	
    	// dd($urlname);
        return view('mainpages.products_category', compact('category'));
    }

    public function products_subcategory($urlname)
    {
        $subcategory = SubCategory::where('url_name', '=', $urlname)->get();
        return view('mainpages.products_subcategory', compact('subcategory'));
    }

    public function specialproduct()
    {
        $myproduct = Special::where('status', '=', 'Active')->get();
        // dd($myproduct);
        return view('mainpages.special_products', compact('myproduct'));
    }
}
