@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">

        <h2 class="my-4" style="text-decoration: underline;">Products SubCategory List</h2>
        
            @if ($message = Session::get('success'))
            <hr>
               <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
               </div>
               <br>
            <hr>
            @endif
        
        

        <div class="card mb-4">
            <div class="card-header">
              <a class="btn btn-sm btn-primary" href="{{ route('subcategory.create') }}">Add New</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                              <th scope="col">Sl No</th>
                              <th scope="col">Category Title</th>
                              <th scope="col">SubCategoryTitle</th>
                              <th scope="col">Url Name</th>
                              <th scope="col">Serial Date</th>
                              <th scope="col">Icons</th>
                              <th scope="col">Content</th>
                              <th scope="col">Status</th>
                              <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th scope="col">Sl No</th>
                              <th scope="col">Category Title</th>
                              <th scope="col">SubCategoryTitle</th>
                              <th scope="col">Url Name</th>
                              <th scope="col">Serial Date</th>
                              <th scope="col">Icons</th>
                              <th scope="col">Content</th>
                              <th scope="col">Status</th>
                              <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($subcategory as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->category->title }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->url_name }}</td>
                                    <td>{{ $item->sl_date }}</td>
                                    <td>
                                      <img class="img-fluid" style="max-height: 80px;" src="{{ asset('uploads/icons/'.$item->icon) }}" alt="Icon">
                                    </td>
                                    <td>{{ $item->content }}</td>
                                    <td {{ $item->status=='Inactive' ? 'style=color:red' : ''}}>{{ $item->status }}</td>
                                    <td>
                                        <a class="btn btn-sm btn-primary" href="{{ url('/admin/subcategory/'.$item->id.'/edit') }}"><i class="fas fa-edit"></i></a>                                        
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>                

@endsection

