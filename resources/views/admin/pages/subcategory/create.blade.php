@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">
        <h3 class="my-4">Add New Product SubCategory</h3>
            <hr>
            {{-- <li class="breadcrumb-item active">Dashboard</li> --}}
            @if ($message = Session::get('success'))
	           <div class="alert alert-success alert-block">
	              <button type="button" class="close" data-dismiss="alert">×</button>
	              <strong>{{ $message }}</strong>
	           </div>
	           <br>
	        @endif
		
            <div class="row">
                <div class="col-md-6">    
                    <form action="{{ url('/admin/subcategory') }}" method="post" enctype="multipart/form-data">
                    @csrf 
                    <div class="form-group">
                        <label for="">Category Title</label>
                        <select class="form-control @error('category_id') is-invalid @enderror" name="category_id" required="required" autofocus>
                          <option value="" active>Select a option</option>
                          @foreach($category as $item)
                          <option value="{{ $item->id }}">{{ $item->title }}</option>
                          @endforeach
                        </select>
                        @error('category_id')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>          
                    <div class="form-group">
                        <label for="">SubCategory Title</label>
                        <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" placeholder="Title" autocomplete="title" required> 
                        @error('title')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">SubCategory Url Name</label> <small>(This name is unique and No special Characters or space)</small>
                        <input type="text" class="form-control @error('url_name') is-invalid @enderror" name="url_name" value="{{ old('url_name') }}" placeholder="Url Name" autofocus autocomplete="url_name" required> 
                        @error('url_name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="">Serial Date</label>
                        <input type="date" class="form-control @error('sl_date') is-invalid @enderror" name="sl_date" value="{{ old('sl_date') }}" autofocus autocomplete="sl_date" required> 
                        @error('sl_date')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <textarea class="form-control" name="content" id="content" rows="4"></textarea>
                        @error('content')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div> 
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="">Status</label>
                        <select class="form-control @error('status') is-invalid @enderror" name="status" required="required">
                          <option value="" active>Select a option</option>
                          <option>Active</option>
                          <option>Inactive</option>
                        </select>
                        @error('status')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="files">SubCategory Icon</label>
                        <input type="file" class="form-control-file @error('icon') is-invalid @enderror" name="icon" required="required" id="files">
                        <small>Icon size 150x150 px</small>
                    </div>
                    <div class="form-group">
                        <label>Selected Image</label><br>
                        <img style="max-height: 150px;" class="img-responsive" id="image" />
                    </div>
                    
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary mb-2">Save</button>
                    <button type="reset" class="btn btn-primary mb-2">Reset</button>
                </div>
            </div>
            </form>
    </div>                

@endsection


@section('javascript')
<script type="text/javascript">
     document.getElementById("files").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("image").src = e.target.result;
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    };
 </script>

 @endsection
