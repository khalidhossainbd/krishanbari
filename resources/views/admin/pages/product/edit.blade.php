@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">
        <h3 class="my-4">Edit Product Info</h3>
        <hr>
        
            {{-- <li class="breadcrumb-item active">Dashboard</li> --}}
            @if ($message = Session::get('success'))
	           <div class="alert alert-success alert-block">
	              <button type="button" class="close" data-dismiss="alert">×</button>
	              <strong>{{ $message }}</strong>
	           </div>
	           <br>
	        @endif
		
        <div class="row">
        	<div class="col-md-6">    
        		<form action="{{ route('product.update',$myproduct->id) }}" method="post" enctype="multipart/form-data">
        		@csrf
        		@method('PUT') 
                <div class="form-group">
                    <label for="">Category Title</label>
                    <select class="form-control @error('category_id') is-invalid @enderror" name="category_id" required="required" autofocus id="demo">
                      <option value="">Select a option</option>
                      @foreach($category as $item)
                      <option {{ $myproduct->category_id == $item->id ? 'selected' : '' }} value="{{ $item->id }}">{{ $item->title }}</option>
                      @endforeach
                    </select>
                    @error('category_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label>SubCategory Title</label>
                    <div>
                    <select class="form-control demo1" name="subcategory_id" required="required">
                      {{--   <option selected disabled="disabled"> --select an option-- </option> --}}
                      <option value="{{ $myproduct->subcategory_id }}">{{ $myproduct->subcategory->title }}</option>
                    </select>
                    @error('subcategory_id')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                    </div>
                </div>     	
        		<div class="form-group">
        		    <label for="">Product Name</label>
        		    <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ $myproduct->title }}" autofocus autocomplete="title" required> 
        		    @error('title')
        		        <span class="invalid-feedback" role="alert">
        		            <strong>{{ $message }}</strong>
        		        </span>
        		    @enderror
        		</div>
                <div class="form-group">
                    <label for="">Product Id</label>
                    <input type="text" class="form-control @error('productId') is-invalid @enderror" name="productId" value="{{ $myproduct->productId }}" autofocus autocomplete="productId" required> 
                    @error('productId')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
        		<div class="form-group">
                    <label for="">Product Url Name</label> <small>(This name is unique and No special Characters or space)</small>
                    <input type="text" class="form-control @error('url_name') is-invalid @enderror" name="url_name" value="{{ $myproduct->url_name }}" autofocus autocomplete="url_name" required> 
                    @error('url_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                
                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea class="form-control" name="content" id="content" rows="4">{{ $myproduct->content }}</textarea>
                    @error('content')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>  
        	</div>
        	<div class="col-md-6">
	    		<div class="form-group">
	    		    <label for="">Status</label>
	    		    <select class="form-control @error('status') is-invalid @enderror" name="status" required="required">
	    		      <option value="" active>Select a option</option>
	    		      <option {{ $myproduct->status == 'Active' ? 'selected' : '' }}>Active</option>
	    		      <option {{ $myproduct->status == 'Inactive' ? 'selected' : '' }}>Inactive</option>
                      <option {{ $myproduct->status == 'Out of Stock' ? 'selected' : '' }}>Out of Stock</option>
	    		    </select>
	    		    @error('type')
	    		        <span class="invalid-feedback" role="alert">
	    		            <strong>{{ $message }}</strong>
	    		        </span>
	    		    @enderror
	    		</div>
                <div class="form-group">
                    <label for="">Units</label> 
                    <input type="text" class="form-control @error('unites') is-invalid @enderror" name="unites" value="{{ $myproduct->unites }}"  autofocus autocomplete="unites" required> 
                    @error('unites')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Unit Price</label> 
                    <input type="text" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ $myproduct->price }}" placeholder="Unit Price" autofocus autocomplete="price" required> 
                    @error('price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Last Price</label> 
                    <input type="text" class="form-control @error('last_price') is-invalid @enderror" name="last_price" value="{{ $myproduct->last_price }}" placeholder="Last Price" autofocus autocomplete="price" required> 
                    @error('last_price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="files">Product Image</label>
                    <input type="file" class="form-control-file @error('images') is-invalid @enderror" name="images" id="files">
                    <small>Icon size 400x400 px</small>
                    @error('images')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Selected Image</label><br>
                    <img style="max-height: 150px;" src="{{ asset('uploads/products/'.$myproduct->images) }}" class="img-responsive" id="image" />
                </div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-6">
        		<button type="submit" class="btn btn-primary mb-2">Update</button>
        	</div>
        </div>
        </form>
    </div>                

@endsection

@section('javascript')

<script type="text/javascript">
    $(document).ready(function(){
        $(document).on('change', '#demo', function(){
        // console.log('It is ok');
        var category_id = $(this).val();
        // console.log(category_id);
        var $this = $(this);
        var op =" ";

        $.ajax({
            type:'get',
            url:'{{ url('/admin/getSubCategory')}}',
            data:{ 'id' : category_id},
            success: function(data){
                op+= '<option value="" selected disabled >--select an option-- </option>';

                for (var i = 0; i < data.length; i++) {
                    op+= '<option value="'+ data[i].id +'">'+ data[i].title + '</option>';
                    }

                $('.demo1').html(op);

            },
            error:function(){

            }
        });
        });
    });
</script>

<script type="text/javascript">
     document.getElementById("files").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("image").src = e.target.result;
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    };
 </script>

@endsection

