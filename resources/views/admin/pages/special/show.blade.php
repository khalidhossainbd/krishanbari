@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">

        <h3 class="my-4" style="text-decoration: underline;">Spacial Product Details</h3>
        
            @if ($message = Session::get('success'))
            <hr>
               <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
               </div>
               <br>
            <hr>
            @endif
        
        

        <div class="card mb-4">
            <div class="card-header">
            	<a class="btn btn-sm btn-primary" href="{{ route('specialproduct.index') }}">Back</a>
              <a class="btn btn-sm btn-info" href="{{ route('specialproduct.edit', $myproduct->id) }}">Edit</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="" width="100%" cellspacing="0">
                        
                        <tr>
                        	<th>Special Product Name</th>
                        	<td>{{ $myproduct->title }}</td>
                        </tr>
                        <tr>
                            <th>Special Product ID</th>
                            <td>{{ $myproduct->productId }}</td>
                        </tr>
                        <tr>
                        	<th>Url Name</th>
                        	<td>{{ $myproduct->url_name }}</td>
                        </tr>
                        <tr>
                            <th>Sl Date</th>
                            <td>{{ $myproduct->sl_date }}</td>
                        </tr>
                        <tr>
                        	<th>Product Unit</th>
                        	<td>{{ $myproduct->unites }}</td>
                        </tr>
                        <tr>
                        	<th>Prodoct Price</th>
                        	<td>{{ $myproduct->price }} (BDT)</td>
                        </tr>
                        <tr>
                        	<th>Prodoct Status</th>
                        	<td>{{ $myproduct->status }}</td>
                        </tr>
                        <tr>
                        	<th>Prodoct Image</th>
                        	<td>
								<img style="max-height: 100px" src="{{ asset('uploads/products/'.$myproduct->images) }}">
                        	</td>
                        </tr>
                        <tr>
                        	<th>Prodoct Description</th>
                        	<td>{{ $myproduct->content }}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div> 
               

@endsection

