@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">

        <h3 class="my-4" style="text-decoration: underline;">Special Products List</h3>
        
            @if ($message = Session::get('success'))
            <hr>
               <div class="alert alert-success alert-block">
                  <button type="button" class="close" data-dismiss="alert">×</button>
                  <strong>{{ $message }}</strong>
               </div>
               <br>
            <hr>
            @endif
        
        

        <div class="card mb-4">
            <div class="card-header">
              <a class="btn btn-sm btn-primary" href="{{ route('specialproduct.create') }}">Add New</a>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-hover" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                              <th scope="col">Sl No</th>
                              <th scope="col">Product Name</th>
                              <th scope="col">Url Name</th>                              
                              <th scope="col">Unit</th>
                              <th scope="col">Price</th>
                              <th scope="col">Status</th>
                              <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                              <th scope="col">Sl No</th>
                              
                              <th scope="col">Product Name</th>
                              <th scope="col">Url Name</th>                              
                              <th scope="col">Unit</th>
                              <th scope="col">Price</th>
                              <th scope="col">Status</th>
                              <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            @foreach($myproducts as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $item->title }}</td>
                                    <td>{{ $item->url_name }}</td>
                                    <td>{{ $item->unites }}</td>
                                    <td>{{ $item->price }}</td>
                                    
                                    <td {{ $item->status=='Inactive' ? 'style=color:red' : ''}}>{{ $item->status }}</td>
                                    <td>
                                      <a class="btn btn-sm btn-info" href="{{ route('specialproduct.show', $item->id) }}"><i class="fas fa-location-arrow"></i></a>
                                        <a class="btn btn-sm btn-primary" href="{{ route('specialproduct.edit', $item->id) }}"><i class="fas fa-edit"></i></a> 
                                        
                                        <form action="{{ route('specialproduct.destroy', $item->id)}}" method="post">
                                          @csrf
                                          @method('DELETE')
                                          <button class="btn btn-sm btn-danger" type="submit">
                                            <i class="fas fa-trash-alt"></i></button>
                                        </form>                                     
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        
    </div>                

@endsection

