@extends('admin.layouts.mainlayout')

@section('content')
        
    <div class="container-fluid">
        <h3 class="my-4">Add New Special Item</h3>
        <hr>
        
            {{-- <li class="breadcrumb-item active">Dashboard</li> --}}
            @if ($message = Session::get('success'))
	           <div class="alert alert-success alert-block">
	              <button type="button" class="close" data-dismiss="alert">×</button>
	              <strong>{{ $message }}</strong>
	           </div>
	           <br>
	        @endif
		
        <div class="row">
        	<div class="col-md-6">    
        		<form action="{{ route('specialproduct.store') }}" method="post" enctype="multipart/form-data">
        		@csrf    
                
                    	
        		<div class="form-group">
        		    <label for="">Special Product Name</label>
        		    <input type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ old('title') }}" placeholder="Special Product Name" autofocus autocomplete="title" required> 
        		    @error('title')
        		        <span class="invalid-feedback" role="alert">
        		            <strong>{{ $message }}</strong>
        		        </span>
        		    @enderror
        		</div>
                <div class="form-group">
                    <label for="">Special Product Id</label>
                    <input type="text" class="form-control @error('productId') is-invalid @enderror" name="productId" value="{{ old('productId') }}" placeholder="Product Id" autofocus autocomplete="productId" required> 
                    @error('productId')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
        		<div class="form-group">
                    <label for="">Special Product Url Name</label> <small>(This name is unique and No special Characters or space)</small>
                    <input type="text" class="form-control @error('url_name') is-invalid @enderror" name="url_name" value="{{ old('url_name') }}" placeholder="Url Name" autofocus autocomplete="url_name" required> 
                    @error('url_name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Serial Date</label>
                    <input type="date" class="form-control @error('sl_date') is-invalid @enderror" name="sl_date" value="{{ old('sl_date') }}" autofocus autocomplete="sl_date" required> 
                    @error('sl_date')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="content">Content</label>
                    <textarea class="form-control" name="content" id="content" rows="4"></textarea>
                    @error('content')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>  
        	</div>
        	<div class="col-md-6">
	    		<div class="form-group">
	    		    <label for="">Status</label>
	    		    <select class="form-control @error('status') is-invalid @enderror" name="status" required="required">
	    		      <option value="" active>Select a option</option>
	    		      <option>Active</option>
	    		      <option>Inactive</option>
                      <option>Out of Stock</option>
	    		    </select>
	    		    @error('type')
	    		        <span class="invalid-feedback" role="alert">
	    		            <strong>{{ $message }}</strong>
	    		        </span>
	    		    @enderror
	    		</div>
                <div class="form-group">
                    <label for="">Units</label> 
                    <input type="text" class="form-control @error('unites') is-invalid @enderror" name="unites" value="{{ old('unites') }}" placeholder="Product Unit" autofocus autocomplete="unites" required> 
                    @error('unites')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="">Unit Price</label> 
                    <input type="text" class="form-control @error('price') is-invalid @enderror" name="price" value="{{ old('price') }}" placeholder="Unit Price" autofocus autocomplete="price" required> 
                    @error('price')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="files">Product Image</label>
                    <input type="file" class="form-control-file @error('images') is-invalid @enderror" name="images" required="required" id="files">
                    <small>Icon size 400x400 px</small>
                    @error('images')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label>Selected Image</label><br>
                    <img style="max-height: 150px;" class="img-responsive" id="image" />
                </div>
        	</div>
        </div>
        <div class="row">
        	<div class="col-md-6">
        		<button type="submit" class="btn btn-primary mb-2">Save</button>
        		<button type="reset" class="btn btn-primary mb-2">Reset</button>
        	</div>
        </div>
        </form>
    </div>                

@endsection

@section('javascript')


<script type="text/javascript">
     document.getElementById("files").onchange = function () {
        var reader = new FileReader();

        reader.onload = function (e) {
            // get loaded data and render thumbnail.
            document.getElementById("image").src = e.target.result;
        };

        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
    };
</script>

@endsection

