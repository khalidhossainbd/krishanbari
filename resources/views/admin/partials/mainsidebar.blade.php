<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                {{-- <div class="sb-sidenav-menu-heading">Core</div> --}}
                <a class="nav-link" href="{{url('/admin/home')}}"
                    ><div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard</a
                >
                <div class="sb-sidenav-menu-heading">Setups</div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts"
                    ><div class="sb-nav-link-icon"><i class="fas fa-user-cog"></i></div>
                    User Control
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                ></a>
                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{ url('/admin/user_list') }}">Admin User List</a>
                        <a class="nav-link" href="{{ url('/admin/register') }}">Create New User</a></nav>
                </div>
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="false" aria-controls="collapsePages"
                    ><div class="sb-nav-link-icon"><i class="fas fa-poll"></i></div>
                    Products Setup
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                ></a>
                <div class="collapse" id="collapsePages" aria-labelledby="headingTwo" data-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav accordion" id="sidenavAccordionPages">
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseAuth" aria-expanded="false" aria-controls="pagesCollapseAuth"
                            >Category
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                        ></a>
                        <div class="collapse" id="pagesCollapseAuth" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ url('/admin/category') }}">Category List</a>
                                <a class="nav-link" href="{{ url('/admin/category/create') }}">Category Add</a>
                                {{-- <a class="nav-link" href="password.html">Forgot Password</a> --}}
                            </nav>
                        </div>
                        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#pagesCollapseError" aria-expanded="false" aria-controls="pagesCollapseError"
                            >Sub-Category
                            <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div
                        ></a>
                        <div class="collapse" id="pagesCollapseError" aria-labelledby="headingOne" data-parent="#sidenavAccordionPages">
                            <nav class="sb-sidenav-menu-nested nav">
                                <a class="nav-link" href="{{ url('/admin/subcategory') }}">Sub-Category List</a>
                                <a class="nav-link" href="{{ url('/admin/subcategory/create') }}">Sub-Category Add</a>
                                {{-- <a class="nav-link" href="500.html">500 Page</a></nav> --}}
                        </div>
                    </nav>
                </div>
                <div class="sb-sidenav-menu-heading">Addons</div>
                <a class="nav-link" href="{{ route('product.create') }}"><div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>Add Products</a>
                <a class="nav-link" href="{{ url('/admin/product') }}"><div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>Products List</a>
                <a class="nav-link" href="{{ url('/admin/specialproduct') }}"><div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>Special Products</a>
                <a class="nav-link" href=""><div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>Products Review</a>

                <div class="sb-sidenav-menu-heading">Page Setup</div>
                <a class="nav-link" href=""><div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>Subscribers List</a>
                <a class="nav-link" href=""><div class="sb-nav-link-icon"><i class="fas fa-chart-area"></i></div>Slider 1 Images</a>
                <a class="nav-link" href=""><div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>Slider 2 Images</a>
                <a class="nav-link" href=""><div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>Advertise</a>
            </div>
        </div>
        <div class="sb-sidenav-footer">
            {{-- <div class="small">Logged in as:</div>
            Start Bootstrap --}}
        </div>
    </nav>
</div>