<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="keywords" content="Krishan bari, Online Shop, Fresh Food, grocery">
    <meta name="author" content="Md Khalid Hossain">
    <link rel="icon" href="{{ asset('assets/dist/images/krishan_logo.png') }}" type="image/gif" sizes="16x16">
    <title>Krishan Bari | Online Shop | @yield('title')</title>

    <link rel="stylesheet" href="{{ asset('assets/node_modules/bootstrap/dist/css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/font-awesome/css/font-awesome.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/node_modules/animate.css/animate.css') }}">

    <link rel="stylesheet" href="{{ asset('assets/dist/css/coustom.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/dist/css/main.min.css') }}">

</head>

<body>

    @include('partials.navber')

    @yield('content')


    @include('partials.footer')


    <script src="{{ asset('assets/node_modules/jquery/dist/jquery.js') }}"></script>
    <script src="{{ asset('assets/node_modules/bootstrap/dist/js/bootstrap.js') }}"></script>
    <script src="{{ asset('assets/dist/js/myjs.js') }}"></script>

    <!-- Load Facebook SDK for JavaScript -->
      {{-- <div id="fb-root"></div>
      <script>
        window.fbAsyncInit = function() {
          FB.init({
            xfbml            : true,
            version          : 'v6.0'
          });
        };

        (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));</script>

      <!-- Your customer chat code -->
      <div class="fb-customerchat"
        attribution=setup_tool
        page_id="110362753871013"
  theme_color="#67b868"
  logged_in_greeting="Welcome to Krishan Bari ! For any query call us in 01400950097. Thanks. "
  logged_out_greeting="Welcome to Krishan Bari ! For any query call us in 01400950097. Thanks. ">
      </div> --}}
</body>

</html>