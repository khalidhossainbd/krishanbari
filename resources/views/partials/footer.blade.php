
    <!-- ============Footer Section Start============== -->

    <section>
        <div class="footer-bg-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-5">
                        <img class="img-fluid" src="{{ asset('assets/dist/images/krishan_logo.png') }}" alt="LOGO">
                    </div>
                    <div class="col-md-5">
                        
                        <div class="input-group">
                            <input type="text" class="form-control subs-btn" placeholder="Enter your email....">
                           
                          </div>
                    </div>
                    <div class="col-md-2">
                            <div class="input-group-append">
                              <button class="btn btn-success btn-block subs-btn" type="submit">Subscribe</button>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bg">
            <div class="container">
                <div class="row">
                    <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                        <h3>Address</h3>
                        <hr class="line-one">
                        <div class="clearfix"></div>
                        <p>
                            Krishan Bari <br>
                            House# 127, Block# B<br>
                            Eastern Housing Pallabi 2nd Phase,<br>
                            Mirpur, Dhaka-1216, Bangladesh.<br><br>
                            Mobile: +880-14 00 950097<br>
                            Email: info@krishanbari.xyz
                        </p>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                        <h3>Useful Links</h3>
                        <hr class="line-one">
                        <div class="clearfix"></div>
                        <ul>
                            <li><a href="">Home</a></li>
                            <li><a href="">Shop</a></li>
                            <li><a href="">Terms & Policies</a></li>
                            <li><a href="">Return Policies</a></li>
                            <li><a href="">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4 col-lg-4">
                        <div class="img-thumbnail">
                            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d2829.752946467027!2d90.3529553690525!3d23.82154714109736!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sbd!4v1588486424142!5m2!1sen!2sbd" width="100%" height="220" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bg-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <p>© Copyright 2020 krishan Bari</p>
                    </div>
                    <div class="col-md-6">
                        <p class="text-right">Design & Developed By <a target="_blank" href="">
                                ARKS IT</a> </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- =================Footer section end ============= -->