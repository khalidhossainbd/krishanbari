

<div class="social-icons">
    <ul>
        <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-whatsapp" aria-hidden="true"></i></a></li>
        <li><a href="#"><i class="fa fa-shopping-basket" aria-hidden="true"></i></a></li>
    </ul>
</div>
<!-- ===========================Navber top section start========================== -->
{{-- <section class="nav-top-section">
    <div class="container-fluid" style="background-color: #37c2d1;">
        <div style="padding: 0 2.3%;">
            <div class="row justify-content-center">
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <p class="px-1 py-2 m-0 lot-line">Hot Line: +880-14 00 950097</p>
                </div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-6">
                    <div class="top-social-icon d-none d-sm-block">
                        <ul>
                            <li><a href="">
                                    <i class="fa fa-facebook-square" aria-hidden="true"></i>
                                </a></li>
                            <li><a href="">
                                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                </a></li>
                            <li><a href="">
                                    <i class="fa fa-linkedin-square" aria-hidden="true"></i>
                                </a></li>
                            <li><a href="">
                                    <i class="fa fa-google-plus-square" aria-hidden="true"></i>
                                </a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<!-- ===========================Navber top section End========================== -->
<section>
    <input type="checkbox" id="mycheck"/>
    <label for="mycheck">
        <i class="fa fa-bars" aria-hidden="true" id="mybtn"></i>
        <i class="fa fa-times" aria-hidden="true" id="mycancel"></i>
    </label>
    <div class="sidebar">
        <header>Krishan Bari</header>
        <ul>
            <li><a href="{{ url('/products/special-products') }}"> <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>     &nbsp;  &nbsp; Special Offers</a></li>
            @foreach($mycategory as $item)
                <li><a href="{{ url('/products/category/'.$item->url_name) }}"><i class="fa fa-arrow-circle-right" aria-hidden="true"></i>  &nbsp;  &nbsp;{{ $item->title }}</a></li>
            @endforeach
            
            {{-- <li><a href="">Grocery</a></li>
            <li><a href="">Home Essential</a></li>
            <li><a href="">Kids Item</a></li>
            <li><a href="">Mans Fashan</a></li> --}}
        </ul>
    </div>
</section>

<!-- ===========================Navber section start========================== -->
<section class="nav-section">
    <div class="px-4" style="margin-left: 50px;">
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="{{ url('/') }}">
                <img src="{{ asset('assets/dist/images/klogo.png') }}" alt="Logo">
            </a>
            <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse"
                data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="{{ url('/') }}">Home <span class="sr-only">(current)</span></a>
                    </li>
                    {{-- <li class="nav-item">
                        <a class="nav-link" href="#">Shop</a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Products
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="#">Action</a>
                            <a class="dropdown-item" href="#">Another action</a>
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" href="#">Something else here</a>
                        </div>
                    </li> --}}
                    
                </ul>
                <form class="form-inline my-2 my-lg-0 my-search-box">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                    <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                </form>
            </div>
        </nav>
    </div>
</section>
<!-- ===========================Navber section End========================== -->