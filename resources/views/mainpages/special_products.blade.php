@extends('layouts.main')

@section('title', 'Products Category')

@section('content')


<!-- ================Product Top Title section Start============== -->

 <section>
     <div class="product-top-bg">
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                 	{{-- @foreach($subcategory as $mytem)
                     	<h1>{{ $mytem->title }}</h1>
                    @endforeach --}}
                    <h1>Special Products</h1>
                     <!-- <hr class="hr-one"> -->
                 </div>                    
             </div>
         </div>
     </div>
 </section>

 <!-- ================Product Top Title section End============== -->



 <!-- ===============Top Main Categories start==================== -->

 <section>
     <div class="container py-4">
         <div class="row">
             <div class="col">
                 {{-- <p class="text-center h2 py-4">Our Special Products Offer</p> --}}
             </div>
         </div>

         <div class="tab-content" id="v-pills-tabContent">
                 
                 <div class="tab-pane show active" id="" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                     <div class="row">
                        @if(count($myproduct)>0)
                            @foreach($myproduct as $products)
                             <div class="col-6 col-sm-4 col-md-3 col-lg-3 mb-3">
                                 <div class="product-holder">
                                     <!-- <p class="on-sale">{{ $products->status == 'Active' ? 'On Sale' : 'Out of Stock' }}</p> -->
                                     <img class="img-fluid" src="{{ asset('uploads/products/'.$products->images) }}" alt="Product Image">
                                     <h5 class="text-center m-1"><b>{{ $products->title }}</b></h5>
                                        <p style="margin: 0px;">{{ $products->unites }}</p>
                                     <p>Price: {{ $products->price }} BDT</p>
                                     <a class="btn btn-block btn-sm btn-outline-primary mb-2" href="" data-toggle="modal" data-target="#pro{{ $products->id }}">Details</a>
                                 </div>
                             </div>

                             <!-- Modal -->
                             
                             <div class="modal fade bd-example-modal-lg" id="pro{{ $products->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"> 
                                 <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                     <div class="modal-content">
                                       <div class="modal-header">
                                         <h5 class="modal-title" id="exampleModalLongTitle">Krishan Bari Premium Products</h5>
                                         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                           <span aria-hidden="true">&times;</span>
                                         </button>
                                       </div>
                                       <div class="modal-body">
                                         <div class="row">
                                             <div class="col-md-5">
                                                 <img class="img-fluid" src="{{ asset('uploads/products/'.$products->images) }}" alt="Product Image">
                                             </div>
                                             <div class="col-md-7">
                                                 <h3>{{ $products->title }}</h3>
                                                 <p>Details:</p>
                                                 <p>{{ $products->content }}</p>
                                                 <p>Unites:</p>
                                                 <p>{{ $products->unites }}</p>
                                                 <p>Price:</p>
                                                 <p>{{ $products->price }}</p>

                                                 <button class="btn btn-primary">For Order Call : +880-14 00 950097</button>
                                             </div>
                                         </div>
                                       </div>
                                       <div class="modal-footer">
                                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                         
                                       </div>
                                     </div>
                                 </div>
                             </div>
                             
                            @endforeach
                        @else

                        <div class="row">
                            <div class="col-12">
                                <p class="text-center h3 py-4">No Offer Available</p>
                            </div>
                        </div>
                            
                        @endif
                     </div>
                 </div>

         </div>
     </div>
 </section>

 <!-- ===============Top Main Categories End==================== -->

 <!-- ================Top Banner Section Start================== -->

 <section>
     <div class="container">
         <div class="row">
             <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                 <img class="img-fluid my-4" src="{{ asset('assets/dist/images/banner-1.jpg') }}" alt="Banner">
             </div>
             <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                 <img class="img-fluid my-4" src="{{ asset('assets/dist/images/banner-2.jpg') }}" alt="Banner">
             </div>
         </div>
     </div>
 </section>

 <!-- ================Top Banner Section End================== -->


@endsection