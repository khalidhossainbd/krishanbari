@extends('layouts.main')

@section('title', 'Products Category')

@section('content')


<!-- ================Product Top Title section Start============== -->

 <section>
     <div class="product-top-bg">
         <div class="container">
             <div class="row">
                 <div class="col-md-12">
                 	@foreach($category as $mytem)
                     	<h1>{{ $mytem->title }}</h1>
                    @endforeach
                     <hr class="hr-one">
                 </div>                    
             </div>
         </div>
     </div>
 </section>

 <!-- ================Product Top Title section End============== -->



 <!-- ===============Top Main Categories start==================== -->

 <section>
     <div class="container">
         <div class="row">
             <div class="col">
                 <p class="text-center h2 py-4">Our Products Categories</p>
             </div>
         </div>
         <div class="row  row-cols-1 row-cols-sm-2 row-cols-md-4">

         	@foreach($category as $item)
         		@foreach($item->subCategory as $subCategory)
		         	<div class="col">
		         	    <div class="top-category text-center">
		         	        <a href="{{ url('/products/subcategory/'.$subCategory->url_name) }}">
		         	        <div class="row">
		         	            <div class="col-3">
		         	                <img class="img-fluid" src="{{ asset('uploads/icons/'.$subCategory->icon) }}" alt="Icon">
		         	            </div>
		         	            <div class="col-9">
		         	                <p class="m-0">{{ $subCategory->title }}</p>
		         	            </div>
		         	        </div>
		         	        </a>
		         	    </div>
		         	</div>
         		@endforeach
         	@endforeach
             
         </div>
     </div>
 </section>

 <!-- ===============Top Main Categories End==================== -->

 <!-- ================Top Banner Section Start================== -->

 <section>
     <div class="container">
         <div class="row">
             <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                 <img class="img-fluid my-4" src="{{ asset('assets/dist/images/banner-1.jpg') }}" alt="Banner">
             </div>
             <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                 <img class="img-fluid my-4" src="{{ asset('assets/dist/images/banner-2.jpg') }}" alt="Banner">
             </div>
         </div>
     </div>
 </section>

 <!-- ================Top Banner Section End================== -->


@endsection