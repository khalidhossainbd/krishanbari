@extends('layouts.main')

@section('content')



<!-- ===========================Navber section End========================== -->

<!-- ======================Top Slider Section Start=================== -->
<section class="slider-section">
    <div class="container-fluid">
        <div class="row">
            <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                    <li data-target="#carouselExampleIndicators" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner">
                    <div class="carousel-item active">
                        <picture>
                          <source media="(max-width:576px)" srcset="{{ asset('assets/dist/images/sliders/one.jpg') }}">
                          <img class="d-block w-100 animated zoomIn" src="{{ asset('assets/dist/images/sliders/sm-slide-04.jpg') }}" alt="First slide">
                        </picture>

                        
                        <div class="carousel-caption d-none d-md-block">
                            {{-- <h5 class="animated zoomInDown delay-1s">Krishan Bari</h5> --}}
                            {{-- <p>Our promice to deliver fresh food</p> --}}
                        </div>
                    </div>
                    <div class="carousel-item">
                        <picture>
                          <source media="(max-width:576px)" srcset="{{ asset('assets/dist/images/sliders/one.jpg') }}">
                          <img class="d-block w-100 animated zoomIn" src="{{ asset('assets/dist/images/sliders/sm-slide-03.jpg') }}" alt="First slide">
                        </picture>
                        
                        <div class="carousel-caption d-none d-md-block">
                            {{-- <h5 class="animated zoomInDown delay-1s">Krishan Bari</h5> --}}
                            {{-- <p>Our promice to deliver fresh food</p> --}}
                        </div>
                    </div>
                    <div class="carousel-item">
                        <picture>
                          <source media="(max-width:576px)" srcset="{{ asset('assets/dist/images/sliders/one.jpg') }}">
                          <img class="d-block w-100 animated zoomIn" src="{{ asset('assets/dist/images/sliders/sm-slide-02.jpg') }}" alt="First slide">
                        </picture>
                        {{-- <img class="d-block w-100 animated zoomInRight" src="{{ asset('assets/dist/images/sliders/sm-slide-02.jpg') }}" alt="Second slide"> --}}
                        <div class="carousel-caption d-none d-md-block">
                            {{-- <h5 class="animated slideInUp delay-1s">Krishan Bari</h5> --}}
                            {{-- <p>Online Shop</p> --}}
                        </div>
                    </div>
                    <div class="carousel-item">
                        <picture>
                          <source media="(max-width:576px)" srcset="{{ asset('assets/dist/images/sliders/one.jpg') }}">
                          <img class="d-block w-100 animated zoomIn" src="{{ asset('assets/dist/images/sliders/sm-slide-01.jpg') }}" alt="First slide">
                        </picture>
                        <img class="d-block w-100" src="{{ asset('assets/dist/images/sliders/sm-slide-01.jpg') }}" alt="Third slide">
                        <div class="carousel-caption d-none d-md-block">
                            {{-- <h5 class="animated zoomInDown delay-1s">Krishan Bari</h5> --}}
                            {{-- <p>Online Shop</p> --}}
                        </div>
                    </div>
                </div>
                <!-- <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button"
                    data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carouselExampleIndicators" role="button"
                    data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a> -->
            </div>
        </div>
    </div>
</section>
<!-- ======================Top Slider Section End=================== -->

<!-- ====================Top Slider Bottom start ======================== -->

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <img class="img-fluid my-3" src="{{ asset('assets/dist/images/one-bg.jpg') }}" alt="">
                {{-- <p class="bg-success p-3 my-2 note-text">
                    Demo Site. This site is under Constraction....
                </p> --}}
            </div>

        </div>
    </div>
</section>

<!-- ====================Top Slider Bottom End ======================== -->

<!-- ===============Top Main Categories start==================== -->

<section>
    <div class="container">
        <div class="row">
            <div class="col">
                <p class="text-center h2 py-4">Our Products Categories</p>
            </div>
        </div>
        <div class="row row-cols-1 row-cols-sm-2 row-cols-md-4">
            <div class="col">
                <div class="top-category text-center">
                    <a href="{{ url('/products/special-products') }}">
                    <div class="row">
                        <div class="col-3">
                            <img class="img-fluid" src="{{ asset('assets/dist/images/icons/buy-home.png') }}" alt="Icon">
                        </div>
                        <div class="col-9">
                            <p class="m-0">Special Offers</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>

            @foreach($mycategory as $item)
            <div class="col">
                <div class="top-category text-center">
                    <a href="{{ url('/products/category/'.$item->url_name) }}">
                    <div class="row">
                        <div class="col-3">
                            <img class="img-fluid" src="{{ asset('uploads/icons/'.$item->icon) }}" alt="Icon">
                        </div>
                        <div class="col-9">
                            <p class="m-0">{{ $item->title }}</p>
                        </div>
                    </div>
                    </a>
                </div>
            </div>
             @endforeach
            
            <!-- <div class="col">
                <div class="top-category text-center">
                    <img class="img-fluid" src="dist/images/icons/diet.png" alt="Icon">
                    <p class="text-center">Grocery</p>
                </div>
            </div> -->
        </div>
    </div>
</section>

<!-- ===============Top Main Categories End==================== -->

<!-- ================Top Banner Section Start================== -->

<section>
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                <img class="img-fluid my-4" src="{{ asset('assets/dist/images/banner-1.jpg') }}" alt="Banner">
            </div>
            <div class="col-12 col-sm-6 col-md-6 col-lg-6">
                <img class="img-fluid my-4" src="{{ asset('assets/dist/images/banner-2.jpg') }}" alt="Banner">
            </div>
        </div>
    </div>
</section>

<!-- ================Top Banner Section End================== -->

<!-- =====================Products tab section start=================== -->
<section class="my-5">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-2">
                <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                    @foreach($subcategory as $subitem)
                    <a class="nav-link {{ $loop->iteration == 1 ? 'active' : '' }}" id="v-pills-home-tab" data-toggle="pill" href="#v{{ $subitem->id }}" role="tab" aria-controls="v-pills-home" aria-selected="true">
                            {{ $subitem->title }}
                    </a>
                    @endforeach

                    {{-- <a class="nav-link active" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile"
                        role="tab" aria-controls="v-pills-profile" aria-selected="false">Grocery</a>
                    <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages"
                        role="tab" aria-controls="v-pills-messages" aria-selected="false">Home Appliances
                    </a>
                    <a class="nav-link" id="v-pills-settings-tab" data-toggle="pill" href="#v-pills-settings"
                        role="tab" aria-controls="v-pills-settings" aria-selected="false">Beauty & Health</a> --}}
                </div>
            </div>
            <div class="col-12 col-sm-10">
                <div class="tab-content" id="v-pills-tabContent">
                    @foreach($subcategory as $subitem)
                        
                        <div class="tab-pane fade {{ $loop->iteration == 1 ? 'show active' : '' }}" id="v{{ $subitem->id }}" role="tabpanel"
                            aria-labelledby="v-pills-profile-tab">
                            <div class="row">
                                @foreach($subitem->product as $products)
                                <div class="col-6 col-sm-4 col-md-3 col-lg-3 mb-3">
                                    <div class="product-holder">
                                       {{--  <p class="on-sale">{{ $products->status == 'Active' ? 'On Sale' : 'Out of Stock' }}</p> --}}
                                        <img class="img-fluid" src="{{ asset('uploads/products/'.$products->images) }}" alt="Product Image">
                                        <h5 class="text-center my-2"><b>{{ $products->title }} </b></h5>
                                        <p style="margin: 0px;">{{ $products->unites }}</p>
                                        <p class="m-0">Price: {{ $products->price }} BDT</p>
                                        <p class="m-0" style="text-decoration: line-through;">Last Price: {{ $products->last_price }} BDT</p>
                                        <a class="btn btn-block btn-sm btn-outline-primary mb-2" href="" data-toggle="modal" data-target="#pro{{ $products->id }}">View More...</a>
                                    </div>
                                </div>

                                <!-- Modal -->
                                
                                <div class="modal fade bd-example-modal-lg" id="pro{{ $products->id }}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"> 
                                    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
                                        <div class="modal-content">
                                          <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLongTitle">Krishan Bari Premium Products</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                              <span aria-hidden="true">&times;</span>
                                            </button>
                                          </div>
                                          <div class="modal-body">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <img class="img-fluid" src="{{ asset('uploads/products/'.$products->images) }}" alt="Product Image">
                                                </div>
                                                <div class="col-md-7">
                                                    <h3>{{ $products->title }}</h3>
                                                    <p>Details:</p>
                                                    <p>{{ $products->content }}</p>
                                                    <p>Unites:</p>
                                                    <p>{{ $products->unites }}</p>
                                                    <p>Price:</p>
                                                    <p>{{ $products->price }}</p>

                                                    <button class="btn btn-primary">For Order Call : +880-14 00 950097</button>
                                                </div>
                                            </div>
                                          </div>
                                          <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            {{-- <button type="button" class="btn btn-primary">Save changes</button> --}}
                                          </div>
                                        </div>
                                    </div>
                                </div>
                                @if ( $loop->iteration == 12)
                                        @break
                                @endif
                                @endforeach
                            </div>
                        </div>

                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>


@endsection