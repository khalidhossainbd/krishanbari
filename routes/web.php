<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('mainpages.home');
// });


Route::get('/', 'MainController@index');

Route::get('/products/category/{url_name}', 'MainController@products_category');
Route::get('/products/subcategory/{url_name}', 'MainController@products_subcategory');

Route::get('/products/special-products', 'MainController@specialproduct');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/admin', 'Admin\Auth\AdminLoginController@showAdminLogin')->name('admin.login');
Route::post('/admin', 'Admin\Auth\AdminLoginController@Login');


Route::get('/admin/home', 'Admin\Auth\AdminController@index');
Route::get('/admin/user_list', 'Admin\Auth\AdminController@userList');
Route::get('/admin/register', 'Admin\Auth\AdminController@register');
Route::post('/admin/register', 'Admin\Auth\AdminController@registerUser');
Route::get('/admin/user/{id}/edit', 'Admin\Auth\AdminController@editUser');
Route::patch('/admin/user/{id}', 'Admin\Auth\AdminController@updateUser');
Route::get('/admin/user/changepassword', 'Admin\Auth\AdminController@changeUserPassword');
Route::patch('/admin/changepassword/{id}', 'Admin\Auth\AdminController@updatePassword');


Route::get('/admin/getSubCategory', 'Admin\ProductController@getSubCategory');

Route::resource('/admin/category', 'Admin\CategoryController');
Route::resource('/admin/subcategory', 'Admin\SubCategoryController');
Route::resource('/admin/product', 'Admin\ProductController');
Route::resource('/admin/specialproduct', 'Admin\SpecialController');